#include <Servo.h>

// config

// mode d'exécution :
// - MODE_TEST : test des actionnauers (1 commande par actionneur)
// - MODE_RUN : mode match
#define MODE_CURRENT MODE_RUN

///////////////////////////////////////////////

// turbine
Servo servoFan;

// casquette
Servo servoDoor;

// funny action
Servo servoFunnyAction;

byte lastCommand = 0;
bool inEmergency = false;
unsigned long funnyActionStart = 0;
byte emergencyCount = 0;

// vert
#define PIN_BIT0 4
// bleu
#define PIN_BIT1 5
// rouge
#define PIN_BIT2 6

#define PIN_EMERGENCY 2

#define PIN_FAN 9
#define PIN_DOOR 10
#define PIN_FUNNY_ACTION 11

#define SERVO_MIN 0
#define SERVO_MAX 180
#define SERVO_NEUTRAL 90

#define POWER_OFF SERVO_NEUTRAL
#define POWER_IN_HOLD 115
#define POWER_IN_MAX SERVO_MAX
#define POWER_OUT_MAX SERVO_MIN

#define DOOR_OPEN 0
#define DOOR_INPUT 45
#define DOOR_OUTPUT 120
#define DOOR_HOLD 83

#define FUNNY_ACTION_INIT 15
#define FUNNY_ACTION_RUN 60

#define COMMAND_EMERGENCY 255

#define EMERGENCY_THRESHOLD 100

#define STATE_DEFAULT 0
#define STATE_PICKUP_AND_HOLD 1
#define STATE_RELEASE 2

// Test des actionneurs (1 commande par actionneur)
#define MODE_TEST 0
// Mode exécution (1 commande par action de jeu)
#define MODE_RUN  10

#define FUNNY_ACTION_TIMEOUT 91000U

void checkFunnyAction() {
  if ((funnyActionStart > 0) && (millis() - funnyActionStart > FUNNY_ACTION_TIMEOUT)) {
    funnyActionStart = 0;
    executeFunnyAction();
  }
}

void startFunnyAction() {
  if (funnyActionStart == 0) {
    funnyActionStart = 1 + millis();
    Serial.print("funny action in ");
    Serial.print(FUNNY_ACTION_TIMEOUT);
    Serial.println(" ms");
  }
}

void executeFunnyAction() {
  Serial.println("funny action");
  servoFunnyAction.write(FUNNY_ACTION_RUN);
}

void readEmergency() {
  bool emergency = digitalRead(PIN_EMERGENCY) != 0;
  if (emergency) {
    if (++emergencyCount > EMERGENCY_THRESHOLD) {
      inEmergency = true;
      digitalWrite(LED_BUILTIN, HIGH);
    }
  } else {
    emergencyCount = 0;
  }
}

void delay_e(unsigned int ms) {
  unsigned long start = millis();
  while (millis() - start < ms && !inEmergency) {
    readEmergency();
    checkFunnyAction();
  }
}

void calibrateFan() {
  servoFan.write(SERVO_MAX);
  delay(5000);
  servoFan.write(SERVO_MIN);
  delay(5000);
  servoFan.write(SERVO_NEUTRAL);
  delay(5000);
}

void testFan() {
  setFan(POWER_OFF);
  delay(5000);
  setFan(POWER_IN_MAX);
  delay(5000);
  setFan(POWER_IN_HOLD);
  delay(5000);
  setFan(POWER_OUT_MAX);
  delay(5000);
  setFan(POWER_OFF);
}

void setup() {
  Serial.begin(115200);

  pinMode(PIN_BIT0, INPUT);
  pinMode(PIN_BIT1, INPUT);
  pinMode(PIN_BIT2, INPUT);

  pinMode(PIN_EMERGENCY, INPUT_PULLUP);
  digitalWrite(PIN_EMERGENCY, HIGH);

  pinMode(LED_BUILTIN, OUTPUT);

  // turbine
  servoFan.attach(PIN_FAN, 1000, 2000);
  setFan(POWER_OFF);

  // casquette
  servoDoor.attach(PIN_DOOR, 100, 2400);
  setDoor(DOOR_OPEN);

  // funny action
  servoFunnyAction.attach(PIN_FUNNY_ACTION);
  servoFunnyAction.write(FUNNY_ACTION_INIT);

  Serial.println("version " __DATE__ " " __TIME__);
  Serial.print("mode: ");
  Serial.println(MODE_CURRENT);

  //calibrateFan();
  //testFan();
  //startFunnyAction();
}

byte readCommand() {
  readEmergency();
  checkFunnyAction();
  if (inEmergency != 0) {
    return COMMAND_EMERGENCY;
  }
  byte b0 = digitalRead(PIN_BIT0);
  byte b1 = digitalRead(PIN_BIT1);
  byte b2 = digitalRead(PIN_BIT2);
  return ((b2 << 2) | (b1 << 1) | b0) + MODE_CURRENT;
}

void setFan(int value) {
  servoFan.write(inEmergency ? POWER_OFF : value);
}

void setDoor(int value) {
  if (!inEmergency) {
    servoDoor.write(value);
  }
}

void executeCommand(byte command) {
  Serial.print("command: ");
  Serial.println(command);
  switch (command) {
    case (MODE_TEST + 0):
      setFan(POWER_OFF);
      break;

    case (MODE_TEST + 1):
      setFan(POWER_IN_HOLD);
      break;

    case (MODE_TEST + 2):
      setFan(POWER_IN_MAX);
      break;

    case (MODE_TEST + 3):
      setFan(POWER_OUT_MAX);
      break;

    case (MODE_TEST + 4):
      setDoor(DOOR_OPEN);
      break;

    case (MODE_TEST + 5):
      setDoor(DOOR_INPUT);
      break;

    case (MODE_TEST + 6):
      setDoor(DOOR_OUTPUT);
      break;

    case (MODE_RUN + 0):
      setState(STATE_DEFAULT);
      break;

    case (MODE_RUN + 1):
      setState(STATE_PICKUP_AND_HOLD);
      break;

    case (MODE_RUN + 2):
      startFunnyAction();
      break;

    case (MODE_RUN + 3):
      setState(STATE_RELEASE);
      break;

    case (MODE_RUN + 4):
      Serial.println("test turbine");
      setFan(POWER_IN_HOLD);
      delay_e(250);
      setFan(POWER_OFF);
      break;

    case COMMAND_EMERGENCY:
      // arrêt d'urgence
      setFan(POWER_OFF);
      Serial.println("EMERGENCY STOP");
      break;
  }
}

void loop() {
  byte command1 = readCommand();
  delay(1);
  byte command2 = readCommand();
  if (command1 != command2) {
    return;
  }
  if (command1 == lastCommand) {
    return;
  }
  lastCommand = command1;
  executeCommand(command1);
}

void setState(byte state) {
  switch (state) {
    case STATE_DEFAULT:
      Serial.println("default");
      setFan(POWER_OFF);
      setDoor(DOOR_OPEN);
      break;

    case STATE_PICKUP_AND_HOLD:
      Serial.println("pickup and hold");
      setFan(POWER_OFF);
      setDoor(DOOR_INPUT);
      delay_e(500);
      setFan(POWER_IN_MAX);
      delay_e(3000);
      setFan(POWER_IN_HOLD);
      setDoor(DOOR_HOLD);
      break;

    case STATE_RELEASE:
      Serial.println("release");
      setDoor(DOOR_OUTPUT);
      delay_e(500);
      setFan(POWER_OUT_MAX);
      delay_e(2000);
      setFan(POWER_OFF);
      delay_e(500);
      setDoor(DOOR_INPUT);
      break;
  }
}

